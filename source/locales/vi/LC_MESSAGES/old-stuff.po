# Vietnamese translation for Release Notes (Old Stuff).
# Copyright © 2009 Free Software Foundation, Inc.
# Clytie Siddall <clytie@riverland.net.au>, 2009.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes 5.0\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-11-05 13:18+0100\n"
"PO-Revision-Date: 2009-02-04 16:55+1030\n"
"Last-Translator: Clytie Siddall <clytie@riverland.net.au>\n"
"Language: vi\n"
"Language-Team: Vietnamese <vi-VN@googlegroups.com>\n"
"Plural-Forms: nplurals=1; plural=0\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.8.0\n"

# | msgid "Managing your |OLDRELEASENAME| system"
#: ../old-stuff.rst:4
#, fuzzy
msgid "Managing your |OLDRELEASENAME| system before the upgrade"
msgstr "Quản lý hệ thống |OLDRELEASENAME|"

#: ../old-stuff.rst:6
msgid ""
"This appendix contains information on how to make sure you can install or"
" upgrade |OLDRELEASENAME| packages before you upgrade to |RELEASENAME|. "
"This should only be necessary in specific situations."
msgstr ""
"Phụ lục này chứa thông tin về cách kiểm tra có thể cài đặt hoặc nâng cấp "
"các gói |OLDRELEASENAME| trước khi nâng cấp lên |RELEASENAME|. Việc kiểm "
"tra này chỉ nên làm trong một số trường hợp nào đó."

#: ../old-stuff.rst:13
msgid "Upgrading your |OLDRELEASENAME| system"
msgstr "Nâng cấp hệ thống |OLDRELEASENAME|"

#: ../old-stuff.rst:15
#, fuzzy
msgid ""
"Basically this is no different from any other upgrade of |OLDRELEASENAME|"
" you've been doing. The only difference is that you first need to make "
"sure your package list still contains references to |OLDRELEASENAME| as "
"explained in `Checking your APT source-list files <#old-sources>`__."
msgstr ""
"Về cơ bản thì việc này không phải khác với nâng cấp bình thường. Sự khác "
"duy nhất là trước tiên người dùng nên kiểm tra danh sách các gói vẫn còn "
"chứa tham chiếu đến |OLDRELEASENAME|, như diễn tả trong `Checking your "
"APT source-list files <#old-sources>`__."

#: ../old-stuff.rst:20
msgid ""
"If you upgrade your system using a Debian mirror, it will automatically "
"be upgraded to the latest |OLDRELEASENAME| point release."
msgstr ""
"Nếu người dùng nâng cấp hệ thống dùng một máy nhân bản Debian thì hệ "
"thống được tự động nâng cấp lên bản phát hành điểm |OLDRELEASENAME| mới "
"nhất."

# | msgid "Checking your sources list"
#: ../old-stuff.rst:26
#, fuzzy
msgid "Checking your APT source-list files"
msgstr "Kiểm tra danh sách nguồn"

#: ../old-stuff.rst:28
#, fuzzy
msgid ""
"If any of the lines in your APT source-list files (see :url-man-"
"stable:`sources.list(5)`) contain references to \"stable\", this is "
"effectively pointing to |RELEASENAME| already. This might not be what you"
" want if you are not yet ready for the upgrade. If you have already run "
"``apt update``, you can still get back without problems by following the "
"procedure below."
msgstr ""
"Nếu tập tin ``/etc/apt/sources.list`` chứa bất cứ dòng nào tham chiếu đến"
" « stable » (bản ổn định) thì kết quả là người dùng đã \"sử dụng\" "
"|RELEASENAME|. Nếu người dùng đã chạy câu lệnh cập nhật ``apt-get "
"update``, vẫn còn có thể trở về mà không gặp vấn đề, bằng cách theo thủ "
"tục dưới đây."

#: ../old-stuff.rst:35
msgid ""
"If you have also already installed packages from |RELEASENAME|, there "
"probably is not much point in installing packages from |OLDRELEASENAME| "
"anymore. In that case you will have to decide for yourself whether you "
"want to continue or not. It is possible to downgrade packages, but that "
"is not covered here."
msgstr ""
"Nếu người dùng cũng đã cài đặt gói từ |RELEASENAME|, rất có thể không có "
"ích khi cài đặt thêm gói từ |OLDRELEASENAME|. Trong trường hợp đó, người "
"dùng cần phải tự quyết định có nên tiếp tục hay không. Cũng có thể hạ cấp"
" gói, nhưng quá trình đó không phải được diễn tả ở đây."

#: ../old-stuff.rst:41
#, fuzzy
msgid ""
"As root, open the relevant APT source-list file (such as "
"``/etc/apt/sources.list``) with your favorite editor, and check all lines"
" beginning with"
msgstr ""
"Hãy mở tập tin ``/etc/apt/sources.list`` dùng trình soạn thảo (với quyền "
"chủ [``root``]) và kiểm tra mọi dòng bắt đầu với ``deb http:`` hay ``deb "
"ftp:`` tìm một tham chiếu đến ``stable`` (bản ổn định). Tìm được thì thay"
" đổi ``stable`` thành |OLDRELEASENAME|."

#: ../old-stuff.rst:45
msgid "``deb http:``"
msgstr ""

#: ../old-stuff.rst:46
msgid "``deb https:``"
msgstr ""

#: ../old-stuff.rst:47
msgid "``deb tor+http:``"
msgstr ""

#: ../old-stuff.rst:48
msgid "``deb tor+https:``"
msgstr ""

#: ../old-stuff.rst:49
msgid "``URIs: http:``"
msgstr ""

#: ../old-stuff.rst:50
msgid "``URIs: https:``"
msgstr ""

#: ../old-stuff.rst:51
msgid "``URIs: tor+http:``"
msgstr ""

#: ../old-stuff.rst:52
msgid "``URIs: tor+https:``"
msgstr ""

#: ../old-stuff.rst:54
#, fuzzy
msgid ""
"for a reference to \"stable\". If you find any, change \"stable\" to "
"\"|OLDRELEASENAME|\"."
msgstr ""
"Hãy mở tập tin ``/etc/apt/sources.list`` dùng trình soạn thảo (với quyền "
"chủ [``root``]) và kiểm tra mọi dòng bắt đầu với ``deb http:`` hay ``deb "
"ftp:`` tìm một tham chiếu đến \"stable\" (bản ổn định). Tìm được thì thay"
" đổi \"stable\" thành |OLDRELEASENAME|."

# | msgid ""
# | "If you have any lines starting with ``deb file:``, you "
# | "will have to check for yourself if the location they refer to contains an
# "
# | "|OLDRELEASENAME| or a |RELEASENAME| archive."
#: ../old-stuff.rst:56
#, fuzzy
msgid ""
"If you have any lines starting with ``deb file:`` or ``URIs: file:``, you"
" will have to check for yourself if the location they refer to contains a"
" |OLDRELEASENAME| or |RELEASENAME| archive."
msgstr ""
"Nếu tập tin đó chứa dòng nào bắt đầu với ``deb file:``, người dùng cần "
"phải tự kiểm tra nếu địa chỉ đã tham chiếu có một kho gói kiểu "
"|OLDRELEASENAME| hay |RELEASENAME|."

# | msgid ""
# | "Do not change any lines that begin with ``deb cdrom:``.  "
# | "Doing so would invalidate the line and you would have to run
# ``apt-"
# | "cdrom`` again.  Do not be alarmed if a 'cdrom' source line refers
# "
# | "to \"``unstable``\".  Although confusing, this
# | "is normal."
#: ../old-stuff.rst:62
#, fuzzy
msgid ""
"Do not change any lines that begin with ``deb cdrom:`` or ``URIs: "
"cdrom:``. Doing so would invalidate the line and you would have to run "
"``apt-cdrom`` again. Do not be alarmed if a ``cdrom:`` source line refers"
" to \"unstable\". Although confusing, this is normal."
msgstr ""
"Không nên thay đổi dòng nào bắt đầu với ``deb cdrom:``. Thay đổi dòng "
"kiểu nào sẽ làm mất hiệu lực dòng đó thì người dùng cần phải chạy lại câu"
" lệnh ``apt-cdrom``. Không cần lo nếu một dòng nguồn « cdrom » tham chiếu"
" đến \"``unstable``\" (bản bất định). Dù trường hợp này làm cho lộn xộn, "
"nó vẫn còn là hợp lệ."

#: ../old-stuff.rst:68
msgid "If you've made any changes, save the file and execute"
msgstr "Sau khi thay đổi gì thì lưu lại tập tin và thực hiện câu lệnh cập nhật"

#: ../old-stuff.rst:74
msgid "to refresh the package list."
msgstr "để cập nhật danh sách các gói."

#: ../old-stuff.rst:79
msgid "Removing obsolete configuration files"
msgstr ""

#: ../old-stuff.rst:81
msgid ""
"Before upgrading your system to |RELEASENAME|, it is recommended to "
"remove old configuration files (such as ``*.dpkg-{new,old}`` files under "
"``/etc``) from the system."
msgstr ""

